#!/bin/bash
set -e

# Warn if the DOCKER_HOST socket does not exist
if [[ $DOCKER_HOST == unix://* ]]; then
	socket_file=${DOCKER_HOST#unix://}
	if ! [ -S $socket_file ]; then
		cat >&2 <<-EOT
			ERROR: you need to share your Docker host socket with a volume at $socket_file
			Typically you should run your jwilder/nginx-proxy with: \`-v /var/run/docker.sock:$socket_file:ro\`
			See the documentation at http://git.io/vZaGJ
		EOT
		socketMissing=1
	fi
fi

# If the user has run the default command and the socket doesn't exist, fail
if [ "$socketMissing" = 1 -a "$1" = forego -a "$2" = start -a "$3" = '-r' ]; then
	exit 1
fi

# Check defaults
if [ -z "${DEFAULT_USER}" ]; then
    echo "DEFAULT_USER is unset or set to the empty string"
    exit 1
fi

if [ -z "${DEFAULT_PASS}" ]; then
    echo "DEFAULT_PASS is unset or set to the empty string"
    exit 1
fi

if [ -z "${NGINX_PROXY_CONTAINER}" ]; then
    echo "NGINX_PROXY_CONTAINER is unset or set to the empty string"
    exit 1
fi


exec "$@"