
Companion container for  jwilder's nginx-proxy.

This container provides helpers to simplify the use of basic auth with [jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy). 
  
## Configuration
 
##### NGINX_PROXY_CONTAINER

Set `NGINX_PROXY_CONTAINER` to the name of your nginx-proxy container. 
This is used to reload the nginx configuration after basic auth config is applied. 

##### DEFAULT_USER

Set `DEFAULT_USER` to your default user name. 
This is used for containers witch have basic auth enabled but provide no user.

##### DEFAULT_PASS

Set `DEFAULT_PASS` to your default password. 
This is used for containers witch have basic auth enabled but provide no password.


## Example
```yaml
nginx-basic-auth:
  build: extended-nginx-proxy-basic-auth
  volumes:
    - /var/run/docker.sock:/tmp/docker.sock:ro
    - ./volumes/htpasswd:/etc/nginx/htpasswd
  environment:
    - NGINX_PROXY_CONTAINER=nginx-proxy
    - DEFAULT_USER=admin
    - DEFAULT_PASS=admin

```
## Configuration for containers

##### VIRTUAL_BASIC_AUTH | false (default)
Set `VIRTUAL_BASIC_AUTH` to true to enable basic auth for a container. 

##### VIRTUAL_BASIC_AUTH_USER | DEFAULT_USER (default)
Set `VIRTUAL_BASIC_AUTH_USER` to the username you will use. 

##### VIRTUAL_BASIC_AUTH_PASS | DEFAULT_PASS (default)
Set `VIRTUAL_BASIC_AUTH_PASS` to the password you will use. 

```yaml

# default user and password 
whoami:
    image: jwilder/whoami
    container_name: whoami
    environment:
      - VIRTUAL_BASIC_AUTH=true
      
# container specific user and password 
whoami:
    image: jwilder/whoami
    container_name: whoami
    environment:
      - VIRTUAL_BASIC_AUTH=true
      - VIRTUAL_BASIC_AUTH_USER=user
      - VIRTUAL_BASIC_AUTH_PASS=pass
```